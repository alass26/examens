<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('token');
            $table->string('from');
            $table->string('to');
            $table->string('exam');
            $table->integer('table_number');
            $table->string('region')->nullable();
            $table->string('state')->nullable();
            $table->integer('sms_id');
            $table->string('sms_content');
            $table->string('send_date');
            $table->string('smsc_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_requests');
    }
}
