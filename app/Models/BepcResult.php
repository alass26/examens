<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BepcResult extends Model
{
    public $fillable = [
        'region',
        'table_number',
        'label',
    ];
}
