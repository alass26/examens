<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CapResult extends Model
{
    public $fillable = [
        'region',
        'table_number',
        'label',
    ];
}
