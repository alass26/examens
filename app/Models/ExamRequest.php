<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExamRequest extends Model
{
    public $fillable = [
        'token',
        'from',
        'to',
        'exam',
        'table_number',
        'region',
        'state',
        'sms_id',
        'sms_content',
        'send_date',
        'smsc_id',
    ];

}
