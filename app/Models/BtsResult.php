<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BtsResult extends Model
{
    public $fillable = [
        'region',
        'table_number',
        'label',
    ];
}
