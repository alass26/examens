<?php

namespace App\Http\Controllers;

use App\Models\BepcResult;
use App\Models\CepdResult;
use App\Models\ExamRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ResultController extends Controller
{
    /**
     * Show the form to send exam's requests
     */
    public function consult_exam_request(Request $request){
        // localhost:8000/results/consult?Token=7sxB17ogLFKfhnfS9Z3HR4Is1twxR4Xu&From=22890992177&To=6767&Examen=BEPC&Num_Tab=330010&Region=Central&Sms_id=11223344&Sms_Cont=BEPC 330010 Central&Date_sms=1999-09-21 14:18&Smsc_id=Togocom
        $input = $request->all();
        //$results = BepcResult::all();

        if (array_key_exists('Token', $input) && $input['Token'] == '7sxB17ogLFKfhnfS9Z3HR4Is1twxR4Xu'){
            if (array_key_exists('Examen', $input) && array_key_exists('Num_Tab', $input)){

                $result = $this->select_request($input['Examen'], $input['Num_Tab']);
                if ($result->isEmpty()){
                    return 'Numéro '. $input['Num_Tab'] .' est introuvable';
                }else{
                    $this->store_request($input);
                }
                if (sizeof($result) == 1){
                    return $result->first()->label;
                }
                if (sizeof($result) != 1){
                    $results = $result->toArray();
                    $first = array_shift($results);

                    foreach ($results as $result){
                       // DB::insert();
                       /* DB::table('send_sms_box')->insert([
                            'email' => 'taylor@example.com',
                            'votes' => 0
                        ]);*/
                       // dump('Send by Kanel');
                       // dump($result->label);
                        $val = true;
                    }
                    return $first->label;
                }

            }else{
                return 'Votre examen n\'est pas renseigner';
            }
        }else{
            return 'Accès non autorisé';
        }

    }

    /**
     * Store the exam's requests
     */
    public function store_request($input){

        return  ExamRequest::create([
            'token' => $input['Token'],
            'from' => $input['From'],
            'to' => $input['To'],
            'exam' => $input['Examen'],
            'table_number' => $input['Num_Tab'],
            'region' => $input['Region'],
           // 'state' => $input['Etat'],
            'sms_id' => $input['Sms_id'],
            'sms_content' => $input['Sms_Cont'],
            'send_date' => $input['Date_sms'],
            'smsc_id' => $input['Smsc_id'],
        ]);
    }

    /**
     * Store the exam's requests
     */
    public function select_request($exam, $number){

        if ($exam == 'CEPD'){
            return CepdResult::where('table_number', $number)->get();
        }
        if ($exam == 'BEPC'){
            return BepcResult::where('table_number', $number)->get();
        }
        if ($exam == 'CEPD'){

        }
    }

}
